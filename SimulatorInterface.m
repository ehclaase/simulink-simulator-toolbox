function varargout = SimulatorInterface(varargin)
% All systems must have been build by SystemBuilder, and have at least 1 valid component.
% Option 1: [sim,strSim,cellStrSysInvalidCmp] = SimulatorInterface('load',<Simulator name>); Loads system variables defined by sim.sysName into sim.sys.
% Option 2: [sim,paramReuse] = SimulatorInterface('nominal',sim,paramReuse); Sets the system parameters to nominal.
% Option 3: [sim,paramReuse] = SimulatorInterface('perturb',sim,paramReuse); Perturbs the system parameters from nominal.

%% initialise
% paths
strPathFile = which('SimulatorBuilder');
oAll = 1:length(strPathFile);
oSlash = oAll(strPathFile == '\');
strPath = strPathFile(1:(oSlash(end)-1));
addpath(strPath);
addpath([strPath,'\Libs']);

%% load systems
if isequal(lower(varargin{1}),'load')
    % check input size
    cIn = length(varargin);
    if cIn ~= 2
        error('Function (varargin{1} = ''load'') undefined for input size ~= 2.');
    end
    
    % get simulator name
    strSim = varargin{2};
    if ~isequal(class(strSim),'char')
        error('Function undefined for input 2 not of class ''char''.');      
    end
    
    % load Simulator variable sim
    try
        load(strSim);
    catch
        error(['Workspace ',strSim,' could not be loaded.']);
    end
    if exist('sim','var') ~= 1
        error(['Simulator workspace ',strSim,' does not contain variable sim.']);
    end
    
    % check that every systems has at least 1 valid variable, and copy struct sys into struct array sim.sys
    if ~isfield(eval('sim'),'sysName')  % load(strSim) loads sim
        error('Simulator variable sim does not contain field sysName.');
    end
    cSys = length(sim.sysName);
    if cSys<1
        error('Variable sim does not contain systems.');
    end
    cellStrSysInvalidCmp = {};
    cellSysPath = cell(cSys);
    for oSys=1:cSys
        % clear variable sys
        clear sys;
        % load system workspace
        try
            load(sim.sysName{oSys});
        catch
            error(['Workspace ''',sim.sysName{oSys},''' could not be loaded.']);
        end
        % check that variable sys is in workspace
        if exist('sys','var') ~= 1
            error(['System workspace ',sim.sysName{oSys},' does not contain variable ''sys''.']);
        end
        % check that there is at least 1 component in 
        [cellStrStrcCmp,cellStrStrcEmpty,strcSysClean] = StrcSysToCellStrCmp(sys);
        if isempty(cellStrStrcCmp)
            error(['Variable ''sys'' in workspace ''',sim.sysName{oSys},''' does not contain any valid components. Please load ''',sim.sysName{oSys},''' in SystemBuilder.m and fix.']);
        end     
        if ~isempty(cellStrStrcEmpty)
            cellStrSysInvalidCmp = {[cellStrSysInvalidCmp{:},':'],sim.sysName{oSys},cellStrStrcEmpty{:},''};
        end
        eval(['sim.sys.',sim.sysName{oSys},' = strcSysClean;']);
        % add system path
        cellSysPath{oSys} = sys.info.dir;
    end    
    
    for oSys=1:cSys
        try
            addpath(genpath(cellSysPath{oSys}));
        catch
            warning(['Could not add system directory ''',cellSysPath{oSys},''' to Matlab path.']);
        end
    end
    
    varargout = {sim,strSim,cellStrSysInvalidCmp};    


%% nominal systems
elseif isequal(lower(varargin{1}),'nominal')   
    % check input size
    cIn = length(varargin);
    if (cIn ~= 2) && (cIn ~= 3)
        error('Function (varargin{1} = ''nominal'') undefined for input size not 2 or 3.');
    end
    
    sim = varargin{2};
    
    if cIn == 3
        paramReuse = varargin{3};
        sim = SimNom(sim,paramReuse); 
        varargout = {sim};
    else
        [sim,paramReuse] = SimNom(sim);   
        varargout = {sim,paramReuse};
    end

%% perturbed system
elseif isequal(lower(varargin{1}),'perturb')
    % check input size
    cIn = length(varargin);
    if (cIn ~= 2) && (cIn ~= 3)
        error('Function (varargin{1} = ''perturb'') undefined for input size not 2 or 3.');
    end
    
    sim = varargin{2};
    
    if cIn == 3
        paramReuse = varargin{3};
        sim = SimPert(sim,paramReuse); 
        varargout = {sim};
    else
        [sim,paramReuse] = SimPert(sim);   
        varargout = {sim,paramReuse};
    end
    
%% unknown option
else
    error('Function undefined for varargin{1} not one of {''load'',''nominal'',''perturb''}.');    
end

end