function child = GetChildBlock(strCmpPtrFile)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
% child: class(child) = 'cell'; child = cell containing list of cmpPtrFile strings of blocks in strCmpPtrFile. 

child={};
if IsLib(strCmpPtrFile)
    child = GetBlock(strCmpPtrFile);
elseif IsDir(strCmpPtrFile)
    lib = GetChildLib(strCmpPtrFile);
    for oLib = 1:length(lib)
        block = GetBlock(lib{oLib});
        for oBlock=1:length(block)
            child{end+1,1} = block{oBlock};
        end
    end    
end

end