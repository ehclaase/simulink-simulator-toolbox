function bIsBlock = IsBlock(strCmpPtrFile)
% Desription:
%   Test if strCmpPtrFile corresponds to an existing Simulink library block, e.g. true if
%       strCmpPtrFile = 'S:\AodcsSimulator\Satellite\Mechanics\Attitude\rigidMech/Euler Law with CMGs'
%       There exists library 'rigidMech' in folder 'S:\AodcsSimulator\Satellite\Mechanics\Attitude\' with block 'Euler Law with CMGs'.
%       strcSys.Satellite.Mechanics.Attitude.EulerCmg.block = 'Satellite\Mechanics\Attitude\rigidMech/Euler Law with CMGs'
% Inputs:
%   strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
% Outputs:
%   bIsBlock: class(bIsBlock) = 'boolean'; bIsBlock = True if strCmp represents a subsystem block within the first level in a library in the current system path. False otherwise.

strLib = StrGetPath(strCmpPtrFile,'slash','/');

if IsLib(strLib)
    cellBlock = GetBlock(strLib);
    
    lBlock = strcmp(cellBlock,strCmpPtrFile);
    
    if sum(lBlock)>0
        bIsBlock = true;
    else
        bIsBlock = false;
    end
else
    bIsBlock = false;
end

end