function par = GetParBlock(strCmpPtrFile)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>)

par = {};

if IsBlock(strCmpPtrFile)
    strLib = StrGetPath(strCmpPtrFile,'slash','/');
   
    load_system(strLib);    
    strBlock = StrRemovePath(strCmpPtrFile);
    hBlock = find_system(strBlock, 'SearchDepth', 1, 'FollowLinks', 'off', 'Type', 'block');
    tBlock = get_param(hBlock,'BlockType');
    close_system(strLib);
    
    for oBlock = 1:length(hBlock)
        if  isequal(tBlock{oBlock},'Inport') && ...
            isequal(lower(GetPortType(hBlock{oBlock})),'par') && ...
            ~isempty(GetPortId(hBlock{oBlock})) && ...
            ~isempty(GetPortSize(hBlock{oBlock}))
            par{end+1,1} = [strCmpPtrFile,'/',hBlock{oBlock}];
        end
    end
end

end