function [strcSysPert,varargout] = SysPert(strcSys,varargin)
    % Description:  Go through system structure and set parameters to perturbed values.
    %               For a specific system, varargout is used for the first call to the function, 
    %               thereafter the varargout is used as varargin such that timely functions are bypassed.
    % Inputs:       strcSys: System structure generated by SystemBuilder.m
    %               varargin: {cellStrStrcCmp,strcSysClean}
    % Outputs:      varargout: {cellStrStrcCmp,strcSysClean}
    
    if isempty(varargin)
        [cellStrStrcCmp,~,strcSysClean] = StrcSysToCellStrCmp(strcSys);
        varargout = {{cellStrStrcCmp,strcSysClean}};
    else
        cellStrStrcCmp = varargin{1}{1};
        strcSysClean = varargin{1}{2};
        varargout={};
    end

    if isempty(strcSysClean)
        error('System does not have basic elements. ''sys'' requirements: Non-empty structure; Field ''info.name''; Field ''info.dir''; Valid directory in field ''info.dir''');
    end    
    
    % Loop through all components
    for oCmp=1:length(cellStrStrcCmp) 
        
        % get component
        strcCmp = eval(['strcSysClean.',cellStrStrcCmp{oCmp},';']);
        cellStrCmpFields = fieldnames(strcCmp);
        
        % Go through all the fields of component
        for oParam=1:length(cellStrCmpFields)        
            % Test if nominal and value fields exist
            if (ExistStruct([cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.val'],strcSysClean) &&... 
                ExistStruct([cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.nom'],strcSysClean) &&... 
                ExistStruct([cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.delta'],strcSysClean) &&... 
                ExistStruct([cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.deltaFunction'],strcSysClean))
                
                % Get parameter attributes
                strNom = mat2str(eval(['strcSysClean.',cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.nom']));
                strDelta = mat2str(eval(['strcSysClean.',cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.delta']));
                strDeltaFunc = eval(['strcSysClean.',cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.deltaFunction']);
                dblSize = eval(['strcSysClean.',cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.size']);
            
                % evaluate Delta Function
                try
                    pert = eval([strDeltaFunc,'(',strNom,',',strDelta,');']);     
                catch
                    error(['Delta Function ',strDeltaFunc,' could not be evaluated with nominal value ',strNom,' and delta value ',strDelta,'.']);
                end
                
                % test pert size
                if ~isequal(size(pert),dblSize)
                    error(['Delta Function ',strDeltaFunc,' output size ',mat2str(size(pert)),' does not match required size of ',mat2str(dblSize),'.']);                    
                end
                
                % set value to perturbation
                eval(['strcSysClean.',cellStrStrcCmp{oCmp},'.',cellStrCmpFields{oParam},'.val = ',mat2str(pert),';']);
            end       
        end
    end
    strcSysPert = strcSysClean;
        
end