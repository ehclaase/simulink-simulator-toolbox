function bIsDir = IsDir(strCmpPtrFile)
% Description: 
%   Test if directory strCmpPtrFile exists, e.g. true if
%       strCmpPtrFile = 'S:\AodcsSimulator\Satellite\Mechanics\Attitude'
%       Directory S:\AodcsSimulator\Satellite\Mechanics\Attitude exists.
% Inputs:
%   strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
% Outputs:
%   bIsDir: class(bIsDir) = 'boolean'; bIsDir = True if strCmpPtrFile represents a directory in the current system path. False otherwise. 

strCmpPtrFileMod = StrRemovePath(strCmpPtrFile);

if ~isequal(strCmpPtrFileMod(1),'.') && (exist(strCmpPtrFile,'dir') == 7)
    bIsDir = true;
else
    bIsDir = false;
end

end