function strCmpPtrStrc = CmpPtrFileToStrc(strCmpPtrFile,strHomeDir,strcSys)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath> or <strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
% strHomeDir: class(strHomeDir) = 'char'; strHomeDir = Home directory string of current system.
% strCmpPtrStrc: class(strCmpPtrStrc) = 'char'; strCmpPtrStrc = Component pointer structure string in standard cmpPtrStrc format, constructed from strCmpPtrFile. (<sysFolderDepth1>.<sysFolderDepth2>. ... <sysFolderDepthN>.<cmpStrcName>)

if IsDir(strCmpPtrFile)
    strBuild = strCmpPtrFile;
    strBuild = strrep(strBuild,[strHomeDir,'\'],'');    % remove strHomeDir from strBuild
    strBuild(strBuild == '\') = '.';
    strCmpPtrStrc = strBuild;
elseif IsBlock(strCmpPtrFile)
    strBuild = strCmpPtrFile;
    strBuild = strrep(strBuild,[strHomeDir,'\'],'');    % remove strHomeDir from strBuild
    strBuild = StrGetPath(strBuild);
    strBuild(strBuild == '\') = '.';
    strCmpPtrStrc = strBuild;
elseif IsCmp(strCmpPtrFile,strHomeDir,strcSys)
    strBuild = strCmpPtrFile;
    strBuild = strrep(strBuild,[strHomeDir,'\'],'');    % remove strHomeDir from strBuild
    lColon = (strBuild == ':');
    oAll = 1:size(lColon,2);
    oColon = oAll(lColon);
    strBuild = [StrGetPath(strBuild),'\',strBuild((oColon+1):end)];
    strBuild(strBuild == '\') = '.';
    strCmpPtrStrc = strBuild;
else
    strCmpPtrStrc = '';
end

end