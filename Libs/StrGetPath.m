function strOut = StrGetPath(strIn,varargin)
    
    % slashes removed from start of string
    % slashes removed from end of string, except possible last slash (only path)
    % slash at end of path not included

    % user options
    oPathDepthTopBottom = nan(1,1);
    slash = '\';
    for oVarArgin = 1:size(varargin,2)
        if isequal('pathDepthTopBottom',varargin{oVarArgin}); oPathDepthTopBottom = varargin{oVarArgin+1}; end;   
        if isequal('slash',varargin{oVarArgin}); slash = varargin{oVarArgin+1}; end;
    end
    
    % remove slashes at start of strPathName
    while (size(strIn,2)>0) && (strIn(1) == slash)
        strIn(1) = [];
    end
    
    % remove slashes at end of strPathName, except possible last slash (only path)
    while (size(strIn,2)>1) && (strIn(end) == slash) && (strIn(end-1) == slash)
        strIn(end) = [];
    end

    if (oPathDepthTopBottom == 0)
        strOut = [];
    elseif isempty(strIn)    
        strOut = [];
    elseif (isnan(oPathDepthTopBottom))
        iSlash = 0;
        for i=1:size(strIn,2)
            if (strIn(i) == slash)
                iSlash = i;
            end
        end
        strOut = strIn(1:iSlash-1);
    else
        lSlash = (strIn == slash);
        iAll = 1:size(strIn,2);
        iSlash = iAll(lSlash);
        if sum(lSlash) < oPathDepthTopBottom
            oPathDepthTopBottom = sum(lSlash);
            warning('Path depth smaller than specified. Full path included.');
        end
        strOut = strIn(1:(iSlash(oPathDepthTopBottom)-1));
    end
end