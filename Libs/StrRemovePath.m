function strOut = StrRemovePath(strIn,varargin)
    
    % slashes removed from start of string
    % slashes removed from end of string, except possible last slash (only path)

    if isequal(class(strIn),'cell')
        strOut{length(strIn)} = [];
        for oStr=1:length(strIn)
            strOut{oStr} = StrRemovePath(strIn{oStr},varargin{:});
        end        
    else    
        % user options
        oPathDepthBottomTop = 0;
        slash = '\';
        for oVarArgin = 1:size(varargin,2)
            if isequal('pathDepthBottomTop',varargin{oVarArgin}); oPathDepthBottomTop = varargin{oVarArgin+1}; end;   
            if isequal('slash',varargin{oVarArgin}); slash = varargin{oVarArgin+1}; end;
        end

        % remove slashes at start of strPathName
        while (size(strIn,2)>0) && (strIn(1) == slash)
            strIn(1) = [];
        end    

        % remove slashes at end of strPathName, except possible last slash (only path)
        while (size(strIn,2)>1) && (strIn(end) == slash) && (strIn(end-1) == slash)
            strIn(end) = [];
        end

        if (oPathDepthBottomTop == 0)
            iSlash = 0;
            for i=1:size(strIn,2)
                if (strIn(i) == slash)
                    iSlash = i;
                end
            end
            strOut = strIn((iSlash+1):end);
        elseif isempty(strIn)
            strOut = [];
        else
            lSlash = (strIn == slash);
            iAll = 1:size(strIn,2);
            iSlash = iAll(lSlash);
            if sum(lSlash) < oPathDepthBottomTop
                oPathDepthBottomTop = sum(lSlash);
                warning('Path depth smaller than specified. Full path included.');
            end
            strOut = strIn((iSlash(end-oPathDepthBottomTop)+1):end);
        end
    end
end