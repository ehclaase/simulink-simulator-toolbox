function strStrc = IndexToStrStrc(iStrc,strc)
% Description:
%   Find the structure path corresponding to structure index iStrc in structure strc, and return as a string, omitting initial 'strc.' from the string.
% Inputs:
%   iStrc: class(iStrc) = 'double'; size(iStrc) = [1,cIStrc]; iStrc = Structure index.
%   strc: class(strc) = 'struct'; length(fields(strc))>0; strc = Structure being indexed. 
% Outputs:
%   strStrc: class(strStrc) = 'string'; strStrc = String containing structure path corresponding to structure index and structure.
% Example:
%   strc.a.a.a=[]; strc.a.a.b=[]; strc.a.a.c=[]; strc.a.b.a=[]; strc.a.b.b=[]; strc.a.b.c=[];
%   IndexToStrStrc([1],strc) = 'a'; IndexToStrStrc([2],strc) = ''; IndexToStrStrc([1,2,3],strc) = 'a.b.c';
% Author: 
%   EH Claase
% Date:
%   2017-04-13

% Test for valid input
if isempty(iStrc) || (sum(iStrc<1)>0) || (sum(abs(iStrc - round(iStrc)))>0) || ... % iStrc must be positive integers
   ~isequal(class(strc),'struct') || isempty(fields(strc)) % strc must be struct with at least 1 field
    strStrc = '';
    return;
end

strStrc = '';
for oiStrc=1:length(iStrc)
    strStrcParent = ['strc',strStrc];
    if ~isequal(class(eval(strStrcParent)),'struct')
        strStrc = '';
        return;
    end
    cellChild = fields(eval(strStrcParent));
    if length(cellChild) < iStrc(oiStrc)
        strStrc = '';
        return;
    end    
    strStrc = [strStrc,'.',cellChild{iStrc(oiStrc)}];    
end
% remove first dot
strStrc(1) = '';    

end