function strType = GetPortType(strPort)

if isequal(class(strPort),'cell')
    strType = cell(size(strPort));
    cPort = length(strType);
    for oPort=1:cPort
        strType{oPort} = GetPortType(strPort{oPort});
    end
else
    strType = StrGetBracketed(strPort,'<','>');
end

end