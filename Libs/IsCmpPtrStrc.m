function bIsCmpPtrStrc = IsCmpPtrStrc(strCmpPtrStrc,strcSys)
% strCmpPtrStrc: class(strCmpPtrStrc) = 'char'; strCmpPtrStrc = Component pointer structure string in standard cmpPtrStrc format. (<sysFolderDepth1>.<sysFolderDepth2>. ... <sysFolderDepthN>.<cmpStrcName>)
% strcSys: class(strcSys) = 'struct'; strcSys = eval(strCmpPtrStrc). Structure reflecting system file structure, containing system components. 
% bIsCmpPtrStrc: class(bIsCmpPtrStrc) = 'boolean'; bIsCmpPtrStrc = True if strCmpPtrStrc is contained in strcSys. False otherwise.

bIsCmpPtrStrc = false;
try
    if isstruct(eval(['strcSys.',strCmpPtrStrc]))
        bIsCmpPtrStrc = true;
    end
catch    
end

end