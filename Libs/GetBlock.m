function block = GetBlock(strCmpPtrFile)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>)
% block: class(block) = 'cell'; block = cell containing list of cmpPtrFile strings of blocks in library strCmpPtrFile. 

block = {};
if IsLib(strCmpPtrFile)
    % get lib info
    load_system(strCmpPtrFile);    
    strLib = StrRemovePath(strCmpPtrFile);
    hBlock = find_system(strLib, 'SearchDepth', 1, 'FollowLinks', 'off', 'Type', 'block');
    tBlock = get_param(hBlock,'BlockType');
    close_system(strCmpPtrFile);
    
    lBlock = strcmp('SubSystem',tBlock);
    strLibPath = StrGetPath(strCmpPtrFile);
    for oBlock = 1:length(lBlock)
        if lBlock(oBlock)
            if isempty(strLibPath)
                block{end+1,1} = hBlock{oBlock};
            else
                block{end+1,1} = [strLibPath,'\',hBlock{oBlock}];
            end
        end
    end         
end

end