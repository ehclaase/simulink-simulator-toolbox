function strBrkd = StrGetBracketed(strEval,strBrkOpen,strBrkClose)

if ~isequal(strBrkOpen,strBrkClose)
    lBrkOpen = (strEval == strBrkOpen);
    lBrkClose = (strEval == strBrkClose);
    if (sum(lBrkOpen) ~= 1) || (sum(lBrkClose) ~= 1)
        strBrkd = '';
        return;
    end
    [~,oBrkOpen] = max(lBrkOpen);
    [~,oBrkClose] = max(lBrkClose);
else
    lBrk = (strEval == strBrkOpen);
    if (sum(lBrk) ~= 2)
        strBrkd = '';
        return;
    end
    oAll = 1:length(strEval);
    oBrk = oAll(lBrk);
    oBrkOpen = oBrk(1);
    oBrkClose = oBrk(2);
end

if oBrkOpen >= oBrkClose
    strBrkd = '';
    return;
end

strBrkd = strEval((oBrkOpen+1):(oBrkClose-1));

end