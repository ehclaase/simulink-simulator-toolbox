function child = GetChild(strCmpPtrFile,varargin)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
% varargin{1} := strHomeDir: class(strHomeDir) = 'char'; strHomeDir = Home directory string of current system.
% varargin{2} := strcSys: class(strcSys) = 'struct'; strcSys = eval(strCmpPtrStrc). Structure reflecting system file structure, containing system components. 
% child: class(child) = 'cell'; child = cell containing list of cmpPtrFile strings of one of subdirectories, libraries or blocks in strCmpPtrFile, whichever is found first. 

if length(varargin) == 2
    strHomeDir = varargin{1};
    strcSys = varargin{2};
end
    
child = GetChildDir(strCmpPtrFile);
if isempty(child)
    child = GetChildBlock(strCmpPtrFile);
    if length(varargin) == 2
        if ~isempty(child)
            childStrc = GetChildStrc(strCmpPtrFile,strHomeDir,strcSys);
            for oChildSys=1:length(childStrc)
                child{end+1,1} = childStrc{oChildSys};
            end
        end
    end
end

end