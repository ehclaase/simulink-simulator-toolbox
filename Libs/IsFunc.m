function bIsFunc = IsFunc(strFunc)

if isequal(class(strFunc),'char')
    if exist(strFunc,'file') == 2
        bIsFunc = true;
    else
        bIsFunc = false;
    end
else
    bIsFunc = false;
end

end