function bExist = ExistStruct(strCmpPtrStrc,strc)

lDot = (strCmpPtrStrc=='.');
oDot = LgcToOrd(lDot);
cDepth = sum(lDot);

bExist = true;
if ~isfield(strc,strCmpPtrStrc(1:(oDot(1)-1)))
    bExist = false;
    return;
end 
for oDepth = 2:cDepth
    if ~isfield(eval(['strc.',strCmpPtrStrc(1:(oDot(oDepth-1)-1))]),strCmpPtrStrc((oDot(oDepth-1)+1):(oDot(oDepth)-1)))
        bExist = false;
        return;
    end        
end
if ~isfield(eval(['strc.',strCmpPtrStrc(1:(oDot(cDepth)-1))]),strCmpPtrStrc((oDot(cDepth)+1):end))
    bExist = false;
    return;
end 

end