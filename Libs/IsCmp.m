function [bIsCmp,varargout] = IsCmp(strCmpPtrFile,strHomeDir,strcSys)
% Desription:
%   Test if strCmpPtrFile exists in the structure strcSys, e.g. true if
%       strCmpPtrFile = 'S:\AodcsSimulator\Satellite\Mechanics\Attitude\rigidMech/Euler Law with CMGs:EulerCmg'
%       strHomeDir = 'S:\AodcsSimulator'
%       strcSys.Satellite.Mechanics.Attitude.EulerCmg.block = 'Satellite\Mechanics\Attitude\rigidMech/Euler Law with CMGs'
% Inputs:
%   strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
%   strHomeDir: class(strHomeDir) = 'char'; strHomeDir = Home directory string of current system.
%   strcSys: class(strcSys) = 'struct'; strcSys = eval(strCmpPtrStrc). Structure reflecting system file structure, containing system components. 
% Outputs:
%   bIsCmp: class(bIsCmp) = 'boolean'; bIsCmp = true if strCmpPtrFile is reflected in strcSys, and the component in strcSys has a field block equal to strCmpPtrFile without strCmpStrcName. False otherwise.

% initialise varargout
varargout = {''};

strBlock = StrGetPath(strCmpPtrFile,'slash',':');

if IsBlock(strBlock)
    % Do not use "strCmpPtrStrc = CmpPtrFileToStrc(strCmpPtrFile,strHomeDir);" here, it contains IsCmp, causing an infinity loop.     
    strBuild = strrep(strCmpPtrFile,[strHomeDir,'\'],'');
    strBuild = [StrGetPath(strBuild,'slash','\'),'.',StrRemovePath(strBuild,'slash',':')];
    strCmpPtrStrc = strrep(strBuild,'\','.');
    
    if IsCmpPtrStrc(strCmpPtrStrc,strcSys)
        try
%             if isequal(eval(['strcSys.',strCmpPtrStrc,'.block']),strrep(strBlock,[strHomeDir,'\'],''))
            if isequal(eval(['strcSys.',strCmpPtrStrc,'.block']),StrRemovePath(strBlock,'slash','\'))
                bIsCmp = true;
                varargout{1} = strCmpPtrStrc;
            else
                bIsCmp = false;
            end
        catch
            bIsCmp = false;
        end        
    else
        bIsCmp = false;
    end
else
    bIsCmp = false;
end

end