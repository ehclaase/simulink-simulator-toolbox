function child = GetChildLib(strCmpPtrFile)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
% child: class(child) = 'cell'; child = cell containing list of cmpPtrFile strings of libraries in strCmpPtrFile. 

child={};
if IsDir(strCmpPtrFile)
    D = dir(strCmpPtrFile);
    for oD=1:size(D,1)
        if IsLib([strCmpPtrFile,'\',D(oD).name(1:(end-4))])
            child{end+1,1} = [strCmpPtrFile,'\',D(oD).name(1:(end-4))];
        end
    end
end

end