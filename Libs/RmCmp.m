function strcRm = RmCmp(strc,strCmpPtrStrc)
% Description: Removes component strCmpPtrStrc from strc, e.g.
%   isfield(strc.Satellite.Mechanics.Attitude,'EulerCmg') = 1
%   isfield(strc.Satellite.Mechanics.Orbit,'Sgp4') = 1
%   strCmpPtrStrc = 'Satellite.Mechanics.Attitude.EulerCmg'
%   isfield(strcRm.Satellite.Mechanics.Attitude,'EulerCmg') = 0
%   isfield(strcRm.Satellite.Mechanics,'Attitude') = 0
%   isfield(strcRm.Satellite.Mechanics.Orbit,'Sgp4') = 1

if ExistStruct(strCmpPtrStrc,strc)
    strCmpPtrStrc = ['.',strCmpPtrStrc];
    lDot = (strCmpPtrStrc=='.');
    oDot = LgcToOrd(lDot);
    cDepth = sum(lDot);
    
    % remove cmp
    strFieldRm = strCmpPtrStrc((oDot(end)+1):end);
    strParentStrc = ['strc',strCmpPtrStrc(1:(oDot(end)-1))];
    eval([strParentStrc,' = rmfield(',strParentStrc,',''',strFieldRm,''');']);
    
    oDepth = cDepth-1;
    while isempty(fields(eval(strParentStrc))) && (oDepth>0)
        strFieldRm = strCmpPtrStrc((oDot(oDepth)+1):(oDot(oDepth+1)-1));
        strParentStrc = ['strc',strCmpPtrStrc(1:(oDot(oDepth)-1))];
        eval([strParentStrc,' = rmfield(',strParentStrc,',''',strFieldRm,''');']);  
        oDepth = oDepth - 1;
    end
end
strcRm = strc;

end