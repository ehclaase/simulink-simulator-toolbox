%% Command list
add_block
add_line
replace_block
isdir

%% Ideas
Rename toolbox to Simulink Simulator Toolbox. The toolbox will have a Build folder and a Simulate folder

%% Development
Create function SimSysSet: This function sets a particular system within the sim structure to a specified instance.
Create function SysDisp: This function displays the system in a readable format.
Create version mark in systems and simulators
Set parameters for fast Rapid acceleration simulation
Create fast SimNom and SimPert functions that reuses the known structure.

%% Requirements
TAI simulation time
make inertial frame selectable
The folder structur of AodcsSimulator resembles subsystems
Suggest Only using the atomic sub-systems
Put identifyers in subsystem port names, either <par> or <var>. Satellite GUI asks for <par> nominal and delta values.
Colour parameters in gui which have not been set
Satellite structure has Sat prefix
Simulation structure has Sim prefix
Directories containing libraries cannot also contain directories, otherwise those libraries are ignored 
The System library existing components should not change.

%% Simulator Interface
function SysNom: This function sets the parameters of a specified system to nominal values
function SysPert: This function sets the parameters of a specified system to perturbed values
function SimNom: This function sets the parameters of a specified simulator to nominal values
function SimPert: This function sets the parameters of a specified simulator to perturbed values

%% Definitions
% Items in the system path is described with full path and name.
% \ signifies subdirectories
% / signifies blocks within a library

%% Question
Should orbit be part of satellite? Yes.
Should environement be part of satellite? Preverably, such that each satellite model can choose which environemtn components to add, but then Simulator layout needs to change, with Environment subsystem block and folder in Satellite.
    Keep folder structure, with SatX01.Satellite and SatX01.Environment.
How can inertial frame be selectable. 

%% Usefull Commands

% Add blocks
% load all libraries first
load_system('AodcsSimulator/Simulator.mdl');
load_system('AodcsSimulator/Satellite/Satellite.mdl');
load_system('AodcsSimulator/Environment/Sun.mdl');
% add blocks
add_block('Satellite/Dummy Satellite','Simulator/Satellite/Satellite1');
add_block('Sun/Basic Sun J2000','Simulator/Environment/Sun1');
% save Simulator
save_system('Simulator');

% Add lines between subsystem blocks and busses

% genpath('V:/Tools/AodcsSimulator')
% 'SearchDepth',1

% read output ports of Satellite block
load_system('AodcsSimulator/Simulator.mdl');
load_system('AodcsSimulator/Satellite/Satellite.mdl');

hSatBlock = find_system('Simulator/Satellite/Satellite1', 'FollowLinks', 'on', 'Type', 'block');
namesSatBlock = get_param(hSatBlock,'BlockType');
lSatBlockInport = strcmp('Inport',namesSatBlock);
iAll = 1:size(lSatBlockInport,1);
iSatBlockInport = iAll(lSatBlockInport);

hSatSys = find_system('Simulator/Satellite', 'Type', 'block');
namesSatSys = get_param(hSatSys,'BlockType');

    % build inport string
inportStr = '';    
for i=iSatBlockInport        
    inportStr = [inportStr,RemovePathFromStr(hSatBlock{i}),','];
end
inportStr(end) = [];

lBusSelector = strcmp('BusSelector',namesSatSys);
hBusSelector = find_system(hSatSys(lBusSelector), 'Type', 'block');

set_param(hBusSelector{:},'OutputSignals',inportStr);

for i=1:size(iSatBlockInport,2)
    add_line('Simulator',[hBusSelector{1},'/',num2str(i)],[GetPathFromStr(hSatBlock{iSatBlockInport(i)}),num2str(i)]);
end


save_system('Simulator');

%
hSatBlock = find_system('rigidBodyRotationalMechanics', 'FollowLinks', 'off', 'Type', 'block');
namesSatBlock = get_param(hSatBlock,'BlockType');
lSatBlockInport = strcmp('SubSystem',namesSatBlock);
temp = {hSatBlock{lSatBlockInport}};




%% Old code


% function [handles] = DisplayComponent(handles)
% 
% folder = FindSubFolder(handles.data.cmp.current);
% if ~isempty(folder)
%     set(handles.Component,'String',folder);
%     set(handles.Component,'Value',size(folder,2));   
%     return;
% end
% 
% libraryBlock = FindSubLibraryBlock(handles.data.cmp.current);
% if ~isempty(libraryBlock)
%     set(handles.Component,'String',libraryBlock);
%     set(handles.Component,'Value',size(libraryBlock,2));
%     return;
% end


% function bIsFolder = IsFolder(cmp)
% 
% if isdir(cmp)
%     bIsFolder = true;
% else
%     bIsFolder = false;
% end

% function bIsLibrary = IsLibrary(cmp)
% 
% if (size(cmp,2) >= 4) && (isequal(cmp((end-3):end),'.mdl') || isequal(cmp((end-3):end),'.slx'))
%     bIsLibrary = true;
% else
%     bIsLibrary = false;
% end
% 
% function bIsBlock = IsBlock(cmp)
% 
% if ~IsFolder(cmp) && IsLibrary(cmp)
%     bIsBlock = true;
% else
%     bIsBlock = false;
% end
% 
% function folder = FindSubFolder(cmp)
% 
% folder = {};
% if IsFolder(cmp)
%     D = dir(cmp);
%     for oD=1:size(D,1)
%         if ~isequal(D(oD).name(1),'.') && (D(oD).isdir == 1)
%             folder = {folder{:},D(oD).name};
%         end
%     end
% end
% 
% function library = FindSubLibrary(cmp)
% 
% library = {};
% if IsFolder(cmp)
%     D = dir(cmp);
%     
%     for oD=1:size(D,1)
%         if IsLibrary(D(oD).name)
%             library = {library{:},D(oD).name};
%         end
%     end
% end  
% 
% function block = FindSubBlock(cmp)
% 
% block = {};
% if IsLibrary(cmp)
%     
%     load_system(cmp);    
%     library = StrRemovePath(cmp);
%     hBlock = find_system(library(1:(end-4)), 'SearchDepth', 1, 'FollowLinks', 'off', 'Type', 'block');
%     nameBlock = get_param(hBlock,'BlockType');
%     lSubSystem = strcmp('SubSystem',nameBlock);
%     if sum(lSubSystem) == 0
%         return;
%     end
%     nameSubSystem = {hBlock{lSubSystem'}}';
%     for oSubSystem = 1:size(nameSubSystem,1)
%         block = {block{:},StrRemovePath(nameSubSystem{oSubSystem},'slash','/')};    
%     end    
%     close_system(cmp);    
% end
% 
% function libraryBlock = FindSubLibraryBlock(cmp)
% 
% libraryBlock = {};
% 
% library = FindSubLibrary(cmp);
% if isempty(library)
%     return;
% end
% for oLib = 1:size(library,2)
%     block = FindSubBlock([cmp,'\',library{oLib}]);
%     for oBlk = 1:size(block,2)
%         libraryBlock = {libraryBlock{:},[library{oLib},'\',block{oBlk}]};
%     end        
% end

% function subCmp = FindSubCmp(currCmp)
% 
% folder = FindFolder(currCmp);
% if ~isempty(folder)
%     subCmp = folder;
%     return;
% end
% 
% libraryBlock = FindLibraryBlock(handles.data.currCmp);
% if ~isempty(libraryBlock)
%     subCmp = libraryBlock;
%     return;
% end
% 
% subCmp = {};


% function [cmpType,cmpId] = GetCmpInfo(cmp)
% % 4 Types are defined for the Component:
% % 'folder' - if cmp is recognized as a directory
% % 'library' - if cmp ends with '.mdl' or '.slx'
% % 
% 
% if isdir(cmp)
%     cmpType = 'folder';
%     cmpId = cmp;
% elseif isequal(cmp((end-3):end),'.mdl') || isequal(cmp((end-3):end),'.slx')
%     cmpType = 'library';
%     cmpId = StrRemovePath(cmp,'slash',handles.pathSlash);
% else
%     oAll = (1:size(cmp,2));
% 
%     lCmpDot = (cmp == '.');
%     oCmpDot = oAll(lCmpDot);
%     if (sum(lCmpDot) ~= 1) || (~isequal(cmp(oCmpDot:(oCmpDot+3)),'.mdl') && ~isequal(cmp(oCmpDot:(oCmpDot+3)),'.slx'))
%         cmpType = '';
%         cmpId = '';
%         return;
%     end            
%     lCmpSlash = [false(1,oCmpDot),(cmp((oCmpDot+1):end) == handles.pathSlash)];
%     if (sum(lCmpSlash) ~= 1)
%         cmpType = '';
%         cmpId = '';
%         return;
%     end
%     
%     cmpType = 'block';
%     cmpId = StrRemovePath(cmp,'pathDepthBottomTop',1,'slash',handles.pathSlash);
% end
%     
%     
%     
%     

%% Simulator GUI
% Bottons: New Simulator; Load Simulator; Update Simulator; Save Simulator; Run Nominal Simulation; Run Monte Carlo Simulations (indexes?.. in the form of function perhaps); Add Scope;    A
%
%
%
%
%
%
%

