function child = GetChildStrc(strCmpPtrFile,strHomeDir,strcSys)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>)
% strHomeDir: class(strHomeDir) = 'char'; strHomeDir = Home directory string of current system.
% strcSys: class(strcSys) = 'struct'; strcSys = eval(strCmpPtrStrc). Structure reflecting system file structure, containing system components. 
% child: class(child) = 'cell'; child = cell containing list of cmpPtrStrc strings of cmp pointed to by strCmpPtrFile in strcSys. 

child = {};
if IsDir(strCmpPtrFile) % strCmpPtrFile must be valid directory       
    strCmpPtrStrc = ['strcSys.',CmpPtrFileToStrc(strCmpPtrFile,strHomeDir)];
    try
        childField = fieldnames(eval(strCmpPtrStrc));
        for oChildField = 1:length(childField)
            if isstruct(eval([strCmpPtrStrc,'.',childField{oChildField}]))
                if isfield(eval([strCmpPtrStrc,'.',childField{oChildField}]),'block')
                    childBlock = GetChildBlock(strCmpPtrFile);                    
%                    lBlock = strcmp(childBlock,[strHomeDir,'\',eval([strCmpPtrStrc,'.',childField{oChildField},'.block'])]);
                    lBlock = strcmp(StrRemovePath(childBlock),eval([strCmpPtrStrc,'.',childField{oChildField},'.block']));
                   if sum(lBlock) > 0                       
                        child{end+1,1} = [childBlock{lBlock},':',childField{oChildField}];
                   end
                end
            end
        end
    catch        
    end
end

end