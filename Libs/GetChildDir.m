function child = GetChildDir(strCmpPtrFile)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
% child: class(child) = 'cell'; child = cell containing list of cmpPtrFile strings of subdirectories in strCmpPtrFile. 

child={};
if IsDir(strCmpPtrFile)
    D = dir(strCmpPtrFile);
    for oD=1:size(D,1)
        if IsDir([strCmpPtrFile,'\',D(oD).name])
            child{end+1,1} = [strCmpPtrFile,'\',D(oD).name];
        end
    end   
end

end