function bIsLib = IsLib(strCmpPtrFile)
% strCmpPtrFile: class(strCmpPtrFile) = 'char'; strCmpPtrFile = Component pointer file string in standard cmpPtrFile format. (<strHomeDir>\<strSysFolderPath>\<strLibName>/<strBlockName>:<strCmpStrcName>)
% IsLib: class(IsLib) = 'boolean'; IsLib = True if strCmpPtrFile represents a library in the current system path. False otherwise. 

bIsLib = (exist(strCmpPtrFile,'file') == 4);

end