function strID = GetPortId(strPort)

if isequal(class(strPort),'cell')
    strID = cell(size(strPort));
    cPort = length(strID);
    for oPort=1:cPort
        strID{oPort} = GetPortId(strPort{oPort});
    end
else
    strID = StrGetBracketed(strPort,'{','}');
end

end