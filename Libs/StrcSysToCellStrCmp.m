function [cellStrStrcCmp,cellStrStrcEmpty,strcSysClean] = StrcSysToCellStrCmp(strcSys)
% Description:
%   Evaluate system structure strcSys, and find all valid components and system structure field that can be removed to leave only valid components, and clean the system structure. 
%   Valid component: Has field block with valid library block name in MATLAB path; 
%                    All block parameters has nominal values in system structure. 

%% initialise
cellStrStrcCmp = {};
cellStrStrcEmpty = {};
strcSysClean = struct();

% test if strcSys is structure and has at least one field, has field 'info.name' and valid field 'info.dir'
if ~isequal(class(strcSys),'struct') || isempty(fields(strcSys)) || (sum(strcmp('info',fields(strcSys)))~=1) || (sum(strcmp('name',fields(eval('strcSys.info'))))~=1) || (sum(strcmp('dir',fields(eval('strcSys.info'))))~=1) || ~IsDir(eval('strcSys.info.dir'))
    return;
end

%% iterate through whole structure
% initialise iStrc
iStrc = 1;
% get structure related to iStrc
strStrcSys = IndexToStrStrc(iStrc,strcSys);

while length(fields(strcSys)) >= iStrc(1)    
    
    if (length(iStrc) == 1) && isequal(strStrcSys,'info')
        iStrc = iStrc + 1;
    else
        % test if valid component, and update index and lists
        if isequal(class(eval(['strcSys.',strStrcSys])),'struct')   % current potential component is struct (possibly cmp)
            cellChild = fields(eval(['strcSys.',strStrcSys]));
            if isempty(cellChild)                                   % current potential component has no fields (not cmp, go to next field)
                cellStrStrcEmpty{end+1} = strStrcSys;
                iStrc(end) = iStrc(end) + 1;
            else                                                    % current potential component has fields (possibly cmp)
                if sum(strcmp('block',cellChild))==1                 % current potential component has field 'block' (possibly cmp)
                    strBlk = eval(['strcSys.',strStrcSys,'.block']);
                    if isequal(class(strBlk),'char')                   % current potential component has field 'block' of class 'char' (possibly cmp)                    
                        strCmpPtrFile = [eval('strcSys.info.dir'),'\',StrGetPath(strrep(strStrcSys,'.','\')),'\',strBlk];                    
                        if IsBlock(strCmpPtrFile)                             % current potential component has field 'block' and is a block (possibly cmp)
                            % Check block parameters
                            strPar = GetParBlock(strCmpPtrFile);
                            bValid = true;
                            for oPar = 1:length(strPar)
                                strId = GetPortId(strPar{oPar});
                                if sum(strcmp(strId,cellChild))~=1
                                    bValid = false;
                                else
                                    if ~isequal(class(eval(['strcSys.',strStrcSys,'.',strId])),'struct')
                                        bValid = false;
                                    else
                                        cellField = fields(eval(['strcSys.',strStrcSys,'.',strId]));
                                        if sum(strcmp('nom',cellField)) ~= 1
                                            bValid = false;                                      
                                        else
                                            if ~isequal(class(eval(['strcSys.',strStrcSys,'.',strId,'.nom'])),'double')
                                                bValid = false;  
                                            end
                                        end
                                    end
                                end
                            end
                            if bValid
                                cellStrStrcCmp{end+1} = strStrcSys;
                                iStrc(end) = iStrc(end) + 1;
                            else
                                cellStrStrcEmpty{end+1} = strStrcSys;   
                                iStrc(end) = iStrc(end) + 1;
                            end
                        else                                        % current potential component has field 'block' but is not a block (not cmp, go to next field)
                            cellStrStrcEmpty{end+1} = strStrcSys;   
                            iStrc(end) = iStrc(end) + 1;
                        end
                    else                                            % current potential component has field 'block' of class other than 'char' (not cmp, go to next field)
                        cellStrStrcEmpty{end+1} = strStrcSys;       
                        iStrc(end) = iStrc(end) + 1;
                    end
                else                                                % current potential component has field 'block' (not cmp, look deeper within struct)
                    iStrc(end+1) = 1;
                end
            end
        else                                                        % current potential component is not struct (not cmp, go to next field)
            cellStrStrcEmpty{end+1} = strStrcSys;
            iStrc(end) = iStrc(end) + 1;
        end
    end
    
    % update current iStrc to next valid iStrc
    strStrcSys = IndexToStrStrc(iStrc,strcSys);
    while isempty(strStrcSys) && (length(iStrc)>1)
        iStrc(end) = [];
        iStrc(end) = iStrc(end)+1;
        strStrcSys = IndexToStrStrc(iStrc,strcSys);
    end
end

%% simplify cellStrStrcEmpty and generate strcSysClean

if ~isempty(cellStrStrcEmpty)
    % reduce depth of each cellStrStrcEmpty element by iterating through each cellStrStrcEmpty element, reducing the depth until it is a substring in cellStrStrcCmp
    for oEmpty=1:length(cellStrStrcEmpty)
        strStrcEmpty = cellStrStrcEmpty{oEmpty};
        bStop = false;
        while ~bStop
            strStrcEmptyMod = StrGetPath(strStrcEmpty,'slash','.');
            if isempty(strStrcEmptyMod)
                break;
            end
            for oCmp=1:length(cellStrStrcCmp)
                if ~isempty(strfind(cellStrStrcCmp{oCmp},strStrcEmptyMod))
                    bStop = true;
                    break;
                end                
            end
            if ~bStop
                strStrcEmpty = strStrcEmptyMod;
            end
        end
        cellStrStrcEmpty{oEmpty} = strStrcEmpty;
    end

    % iterate through reduced cellStrStrcEmpty and remove redundant structure paths
    oEmpty1 = 1;
    while oEmpty1 <= length(cellStrStrcEmpty)
        bRemove = false;
        for oEmpty2 = 1:length(cellStrStrcEmpty)   
            if (oEmpty1 ~= oEmpty2) && ~isempty(strfind(cellStrStrcEmpty{oEmpty1},cellStrStrcEmpty{oEmpty2}))
                bRemove = true;
                break;
            end
        end
        if bRemove
            cellStrStrcEmpty(oEmpty1) = [];
        else
            oEmpty1 = oEmpty1 + 1;
        end
    end

    % build clean system structure
    strcSysClean.info = strcSys.info;
    for oCmp=1:length(cellStrStrcCmp)
        eval(['strcSysClean.',cellStrStrcCmp{oCmp},' = strcSys.',cellStrStrcCmp{oCmp},';'])
    end
else    % bypass above if cellStrStrcEmpty is empty
    strcSysClean = strcSys;
end

end