function cellStrDeltaFunction = StrcSysToCellStrDeltaFunction(strcSys)
% Description:
%   Evaluate system structure strcSys, and find all DeltaFunctions.

%% initialise

% test if strcSys is structure and has at least one field, has field 'info.name' and valid field 'info.dir'
if ~isequal(class(strcSys),'struct') || isempty(fields(strcSys)) || (sum(strcmp('info',fields(strcSys)))~=1) || (sum(strcmp('name',fields(eval('strcSys.info'))))~=1) || (sum(strcmp('dir',fields(eval('strcSys.info'))))~=1) || ~IsDir(eval('strcSys.info.dir'))
    return;
end

%% iterate through whole structure
% initialise output
cellStrDeltaFunction = {};
% initialise iStrc
iStrc = 1;
% get structure related to iStrc
strStrcSys = IndexToStrStrc(iStrc,strcSys);

while length(fields(strcSys)) >= iStrc(1)    
    
    if (length(iStrc) == 1) && isequal(strStrcSys,'info')
        iStrc = iStrc + 1;
    else
        % test if deltaFunction, and update index and lists
        if isequal(class(eval(['strcSys.',strStrcSys])),'struct')   % current potential deltaFunction parent is struct (possibly deltaFunction)
            cellChild = fields(eval(['strcSys.',strStrcSys]));
            if isempty(cellChild)                                   % current potential deltaFunction parent has no fields (not deltaFunction, go to next field)
                iStrc(end) = iStrc(end) + 1;
            else                                                    % current potential deltaFunction parent has fields (possibly deltaFunction)
                lIsChildDeltaFunction = strcmp('deltaFunction',cellChild);
                if sum(lIsChildDeltaFunction)==1                    % current potential deltaFunction parent has field deltaFunction
                    % only add Delta Function to cell if not already added
                    strDeltaFunction = eval(['strcSys.',strStrcSys,'.',cellChild{lIsChildDeltaFunction}]);
                    if sum(strcmp(cellStrDeltaFunction,strDeltaFunction)) == 0
                        cellStrDeltaFunction{end+1} = strDeltaFunction;
                    end
                    iStrc(end) = iStrc(end) + 1;
                else                                                % current potential deltaFunction parent has no fields deltaFunction (not deltaFunction, go level deeper in struct)
                    iStrc(end+1) = 1;
                end
            end
        else                                                        % current potential deltaFunction parent is not struct (not deltaFunction, go to next field)
            iStrc(end) = iStrc(end) + 1;
        end
    end
    
    % update current iStrc to next valid iStrc
    strStrcSys = IndexToStrStrc(iStrc,strcSys);
    while isempty(strStrcSys) && (length(iStrc)>1)
        iStrc(end) = [];
        iStrc(end) = iStrc(end)+1;
        strStrcSys = IndexToStrStrc(iStrc,strcSys);
    end
end


end