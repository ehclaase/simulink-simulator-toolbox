function varargout = SystemBuilder(varargin)
% SYSTEMBUILDER MATLAB code for SystemBuilder.fig
%      SYSTEMBUILDER, by itself, creates a new SYSTEMBUILDER or raises the existing
%      singleton*.
%
%      H = SYSTEMBUILDER returns the handle to a new SYSTEMBUILDER or the handle to
%      the existing singleton*.
%
%      SYSTEMBUILDER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SYSTEMBUILDER.M with the given input arguments.
%
%      SYSTEMBUILDER('Property','NominalValue',...) creates a new SYSTEMBUILDER or raises the
%      existing singleton*.  Starting from the left, property nominalvalue pairs are
%      applied to the GUI before SystemBuilder_OpeningFcn gets called.  An
%      unrecognized property name or invalid nominalvalue makes property application
%      stop.  All inputs are passed to SystemBuilder_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SystemBuilder
% Last Modified by GUIDE v2.5 02-Apr-2017 12:49:56

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SystemBuilder_OpeningFcn, ...
                   'gui_OutputFcn',  @SystemBuilder_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before SystemBuilder is made visible.
function SystemBuilder_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SystemBuilder (see VARARGIN)
% Load folder directory for "Components"

% Initialise path
strPathFile = which('SystemBuilder');
oAll = 1:length(strPathFile);
oSlash = oAll(strPathFile == '\');
strPath = strPathFile(1:(oSlash(end)-1));
addpath(strPath);
addpath([strPath,'\Libs']);

% Initialise GUI variables
handles.data.sys = struct();
handles.data.sys.info.name = '';
handles.data.sys.info.dir = '';
handles.data.cmp = ''; 
handles.data.strSysDir = ''; 
handles.data.strDeltaFunction = {};
handles = UpdateCmpBrowser(handles);

% Choose default command line output for SystemBuilder
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes SystemBuilder wait for user response (see UIRESUME)
% uiwait(handles.figure1);
return;


% --- Outputs from this function are returned to the command line.
function varargout = SystemBuilder_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in ComponentBrowser.
function ComponentBrowser_Callback(hObject, eventdata, handles)
% hObject    handle to ComponentBrowser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns ComponentBrowser contents as cell array
%        contents{get(hObject,'NominalValue')} returns selected item from ComponentBrowser
handles = guidata(hObject);
childList = get(hObject,'String');
child = childList{get(hObject,'Value')};
child = SetChild(handles.data.cmp,child);
if IsDir(child)
    childChild = GetChild(child);
    if ~isempty(childChild)
        handles.data.cmp = child;
        handles = UpdateCmpBrowser(handles);
    end
end
handles = UpdateParBrowser(handles);
guidata(hObject,handles);
return;


% --- Executes during object creation, after setting all properties.
function ComponentBrowser_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ComponentBrowser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in NewSystem.
function NewSystem_Callback(hObject, eventdata, handles)
% hObject    handle to NewSystem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles = guidata(hObject);
handles = NewSystem(handles);
guidata(hObject,handles);
return;


% --- Executes during object creation, after setting all properties.
function NewSystem_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NominalValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function NominalValue_Callback(hObject, eventdata, handles)
% hObject    handle to NominalValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of NominalValue as text
%        str2double(get(hObject,'String')) returns contents of NominalValue as a double


% --- Executes during object creation, after setting all properties.
function NominalValue_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NominalValue (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in LoadSystem.
function LoadSystem_Callback(hObject, eventdata, handles)
    % hObject    handle to LoadSystem (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
   
    % get gui data
    handles = guidata(hObject);
    
    % User promp: Select existing system mat file
    [strFile, strPath] = uigetfile('*.mat', 'Load System');
    % Test if cancel pressed
    if isequal(strFile,0)
        return;
    end
    % Test if mat-file selected
    if ~isequal(strFile((end-3):end),'.mat')
        uiwait(msgbox('User must select a mat-file','Load System','help'));
        return;
    end
    
    % load mat-file
    load([strPath,'\',strFile]);
    
    % test if 'sys' structure included
    if ~exist('sys','var')
        uiwait(msgbox('Mat-file selected must contain system structure ''sys''','Load System','help'));
        return;
    end
    
    % Test if system has non-standard fields
    bLoop = true;
    while bLoop
        % Get loaded system status
        [~,cellStrStrcEmpty,strcSysClean] = StrcSysToCellStrCmp(sys);
        if isempty(fields(strcSysClean))
            uiwait(msgbox({'System Loaded does not have basic elements.','''sys'' requirements:','-Non-empty structure','-Field ''info.name''','-Field ''info.dir''','-Valid directory in field ''info.dir'''},'Load System','none'));
            return;
        end
        if ~isempty(cellStrStrcEmpty)
            [oSelect,bSelect] = listdlg('ListString',{'Change System Directory','Clean System','Continue (Not Recommended)','Abort Load'},'SelectionMode','single','InitialValue',[1],'Name','Load System','PromptString',{'The system selected to load has unresolved fields:',cellStrStrcEmpty{:}},'ListSize',[300,300]);
            if bSelect == 0
                uiwait(msgbox('System Load Cancelled','Load System','none'));
                return;
            end
            if oSelect ~= 1
                bLoop = false;
                if (oSelect == 2)
                    sys = strcSysClean;
                elseif (oSelect == 3)
                    sys = sys;
                else
                    return;
                end                
            else
                strSysDir = uigetdir(pwd,'Change System Directory');
                if strSysDir == 0
                    uiwait(msgbox('System Directory Change Cancelled','Load System','none'));
                    return;
                end
                sys.info.dir = strSysDir;
            end
        else
            bLoop = false;
        end
    end   
    
    % Remove previous system path
    rmpath(genpath(handles.data.strSysDir));
    
    % Update handles.data
    handles.data.sys = sys;
    handles.data.strSysDir = sys.info.dir;   
    addpath(genpath(handles.data.strSysDir));
    handles.data.cmp = handles.data.strSysDir;  
    handles.data.strDeltaFunction = StrcSysToCellStrDeltaFunction(handles.data.sys);
    set(handles.SystemName,'String',sys.info.name);
    handles = UpdateCmpBrowser(handles);
    guidata(hObject,handles);
    return;

% --- Executes on button press in SaveSystem.
function SaveSystem_Callback(hObject, eventdata, handles)
    % hObject    handle to SaveSystem (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    handles = guidata(hObject);     % Requirement: Cannot save if sys does not have home directory
    sys = handles.data.sys;
    if isempty(sys.info.name)
        sysName = ['System ',datestr(now)];
        if ~isequal('Continue',questdlg({'The current system does not have a name.','If you continue, the name ',sysName,' will be given.'},'Save System','Continue','Abort','Continue'))
            uiwait(msgbox('System not saved','Save System','warn'));
            return;
        end
        sys.info.name = sysName;
    end
    sys.info.date = datestr(now);
    sys.info.dir = handles.data.strSysDir;
    sys.info.origin = ['SystemBuilder ',SimulatorToolboxVersion()];
    % save(['Sat',aodcsVar.name,'.mat'], 'aodcsVar');    
    %[fileName, pathName] = uiputfile('System.mat', 'Save System as');    
    strSaveDir = uigetdir(pwd,'Select Save Directory');
    if isequal(strSaveDir,0)
        uiwait(msgbox({'Select Save Directory Usage:','User must select existing directory.'},'Invalid User Input','help'));
        uiwait(msgbox('System not saved','Save System','warn'));
        return;
    end    
    save([strSaveDir,'\',sys.info.name,'.mat'], 'sys');
    return;

% --- Executes on selection change in ParameterBrowser.
function ParameterBrowser_Callback(hObject, eventdata, handles)
% hObject    handle to ParameterBrowser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns ParameterBrowser contents as cell array
%        contents{get(hObject,'NominalValue')} returns selected item from ParameterBrowser
    % get data
    handles = guidata(hObject);
    % update browser
    handles = UpdateParAttribute(handles);
    % set data
    guidata(hObject,handles);
    return;
    
    
% --- Executes during object creation, after setting all properties.
function ParameterBrowser_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ParameterBrowser (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in BackComponent.
function BackComponent_Callback(hObject, eventdata, handles)
% hObject    handle to BackComponent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    handles = guidata(hObject);
    if ~isequal(handles.data.cmp,handles.data.strSysDir)
        handles.data.cmp = SetParent(handles.data.cmp);
    end
    handles = UpdateCmpBrowser(handles);
    guidata(hObject,handles);
    return;


% --- Executes on button press in AddComponent.
function AddComponent_Callback(hObject, eventdata, handles)
    % hObject    handle to AddComponent (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Get handles
    handles = guidata(hObject);
    % Get Component selected
    cellCmpList = get(handles.ComponentBrowser,'String');
    strCmpSelect = cellCmpList{get(handles.ComponentBrowser,'Value')};
    strCmpPtrFile = [handles.data.cmp,'\',strCmpSelect];
    % Can only add component if a block was selected
    if IsBlock(strCmpPtrFile)
        % Get new component name from user
        cellNewCmp = inputdlg('Enter New Component Name','User Input');    
        if isempty(cellNewCmp)
            return;
        end
        strNewCmp = genvarname(cellNewCmp{1});

        % Test if new component name already exists, and return if it does.
        sys = handles.data.sys;
        strCmpPtrStrc = ['sys.',CmpPtrFileToStrc(strCmpPtrFile,handles.data.strSysDir),'.',strNewCmp];
        try
            eval([strCmpPtrStrc,';']);
            uiwait(msgbox('Component name already exists','Invalid User Input','warn'));
            return;
        catch
        end

        % Create new component in sys structure
        eval([strCmpPtrStrc,'.block=''',strCmpSelect,''';']);
        % Fill new component parameters with zeros
        par = GetParBlock(strCmpPtrFile);
        for oPar=1:length(par)
			strParSize = GetPortSize(par{oPar});
            strParSizeCompact = strrep(strrep(strParSize,' ',''),'x','');
            if ~isempty(str2double(strParSizeCompact))
                strParSizeDbl = ['[',strrep(strrep(strParSize,' ',''),'x',','),']'];
                dblParSize = str2num(strParSizeDbl);
                eval([strCmpPtrStrc,'.',GetPortId(par{oPar}),'.size=',strParSizeDbl,';']);
            else
                dblParSize = [];
                eval([strCmpPtrStrc,'.',GetPortId(par{oPar}),'.size=''',strSize,''';']);
            end
            eval([strCmpPtrStrc,'.',GetPortId(par{oPar}),'.nom=',mat2str(zeros(dblParSize)),';']);
            eval([strCmpPtrStrc,'.',GetPortId(par{oPar}),'.val=',mat2str(zeros(dblParSize)),';']);
            eval([strCmpPtrStrc,'.',GetPortId(par{oPar}),'.unit=''',strrep(GetPortUnit(par{oPar}),'//','/'),''';']); % Matlab replace slash in name with double slash
        end
        handles.data.sys = sys;

        % Update component browser with new component
        handles = UpdateCmpBrowser(handles);

        % Highlight new component in browser for quick access to parameters 
        cellCmpList = get(handles.ComponentBrowser,'String');
        oNewCmp = LgcToOrd(strcmp(cellCmpList,[strCmpSelect,':',strNewCmp]));
        set(handles.ComponentBrowser,'Value',oNewCmp);

        % Update parameter browser
        handles = UpdateParBrowser(handles);

        % Store data
        guidata(hObject,handles);
    else
        uiwait(msgbox({'Add Component Usage:','Select library block in the Component Browser','Press the Add Component button'},'Invalid User Input','help'));
    end
    % handles = DisplayParChild(handles);

% --- Executes on button press in RemoveComponent.
function RemoveComponent_Callback(hObject, eventdata, handles)
    % hObject    handle to RemoveComponent (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    handles = guidata(hObject);
    
    strCmpPtrFile = GetSelectComponent(handles);
    if IsCmp(strCmpPtrFile,handles.data.strSysDir,handles.data.sys)        
        strCmpPtrStrc = CmpPtrFileToStrc(strCmpPtrFile,handles.data.strSysDir,handles.data.sys);
        handles.data.sys = RmCmp(handles.data.sys,strCmpPtrStrc);        
        handles = UpdateCmpBrowser(handles);
    else
        uiwait(msgbox({'Remove Component Usage:','Select added component in the Component Browser','Press the Remove Component button'},'Invalid User Input','help'));        
    end
    
    % save gui data
    guidata(hObject,handles);
    return;

% --- Executes on selection change in UnitSize.
function Unit_Callback(hObject, eventdata, handles)
    % hObject    handle to UnitSize (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % Hints: contents = cellstr(get(hObject,'String')) returns UnitSize contents as cell array
    %        contents{get(hObject,'NominalValue')} returns selected item from UnitSize


% --- Executes during object creation, after setting all properties.
function UnitSize_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to UnitSize (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    % Hint: popupmenu controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


function SystemName_Callback(hObject, eventdata, handles)
    % hObject    handle to SystemName (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % Hints: get(hObject,'String') returns contents of SystemName as text
    %        str2double(get(hObject,'String')) returns contents of SystemName as a double
    handles = guidata(hObject);
    handles.data.sys.info.name = get(hObject, 'String');
    guidata(hObject,handles);
    return;


% --- Executes during object creation, after setting all properties.
function SystemName_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to SystemName (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called
    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on selection change in DeltaFunctionBrowser.
function DeltaFunctionBrowser_Callback(hObject, eventdata, handles)
    % hObject    handle to DeltaFunctionBrowser (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: contents = cellstr(get(hObject,'String')) returns DeltaFunctionBrowser contents as cell array
    %        contents{get(hObject,'Value')} returns selected item from DeltaFunctionBrowser
    
    % get gui data
    handles = guidata(hObject);
    
    % test if user selected '<add>' from dropdown menu
    getDeltaFuncList = get(handles.DeltaFunctionBrowser,'String');
    if isequal(class(getDeltaFuncList),'cell')
        strDeltaFuncSelect = getDeltaFuncList{get(handles.DeltaFunctionBrowser,'Value')};
    else
        strDeltaFuncSelect = getDeltaFuncList;
    end
    if isequal(strDeltaFuncSelect,'<add>')
        strDeltaFunc = uigetfile(handles.data.strSysDir,'Select Delta Function');
        if IsFunc(strDeltaFunc)
            strDeltaFunc = StrGetPath(strDeltaFunc,'slash','.');    % remove .m from name
            % test if Delta Function already included in list 
            if sum(strcmp(strDeltaFunc,handles.data.strDeltaFunction)) > 0            
                uiwait(msgbox({'Select Data Function Usage:','Cannot add delta function which is already in dropdown menu'},'Invalid User Input','help'));  
            else
                handles.data.strDeltaFunction = {strDeltaFunc,handles.data.strDeltaFunction{:}};
            end
        else
            uiwait(msgbox({'Select Delta Function Usage:','Must select valid matlab function'},'Invalid User Input','help'));
        end
        % only update delta function browser if component is added.
        handles = UpdateDeltaFunctionBrowser(handles);
    end
    
    % save gui data
    guidata(hObject,handles);
    return;

% --- Executes during object creation, after setting all properties.
function DeltaFunctionBrowser_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to DeltaFunctionBrowser (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: popupmenu controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    

function DeltaValue_Callback(hObject, eventdata, handles)
    % hObject    handle to DeltaValue (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Hints: get(hObject,'String') returns contents of DeltaValue as text
    %        str2double(get(hObject,'String')) returns contents of DeltaValue as a double


% --- Executes during object creation, after setting all properties.
function DeltaValue_CreateFcn(hObject, eventdata, handles)
    % hObject    handle to DeltaValue (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    empty - handles not created until after all CreateFcns called

    % Hint: edit controls usually have a white background on Windows.
    %       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end

% --- Executes on button press in DeltaHelp.
function DeltaHelp_Callback(hObject, eventdata, handles)
    % hObject    handle to DeltaHelp (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % get gui data
    handles = guidata(hObject);
    
    % get Delta function
    getDeltaFuncList = get(handles.DeltaFunctionBrowser,'String');
    if isequal(class(getDeltaFuncList),'cell')
        strDeltaFuncSelect = getDeltaFuncList{get(handles.DeltaFunctionBrowser,'Value')};
    else
        strDeltaFuncSelect = getDeltaFuncList;
    end
    
    % display help
    if isequal(strDeltaFuncSelect,'Delta Function Browser')
        cellStrHelp = {'To add variation to a system parameter:','-Select a Delta function from the Delta Function Browser drop-down list.','-Set the Delta Value.','-Press the Set Parameter button.','To add a Delta function,','select <add> from the Delta Function Browser drop-down list'};
    elseif isequal(strDeltaFuncSelect,'<add>')
        cellStrHelp = {'To add a Delta function,','select <add> from the Delta Function Browser drop-down list'};
    else
        strHelp = help(strDeltaFuncSelect);
        cellStrHelp = {[strDeltaFuncSelect,':'],strHelp};
    end
    uiwait(msgbox(cellStrHelp,'Delta Function Browser Help','help'));
    return
    
% --- Executes on button press in SetParameter.
function SetParameter_Callback(hObject, eventdata, handles)
    % hObject    handle to SetParameter (see GCBO)
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    
    % get gui data
    handles = guidata(hObject);
    
    % get selected parameter from Parameter Browser
    strParPtrStrc = GetSelectParameter(handles);
    if isempty(strParPtrStrc)
        uiwait(msgbox({'Set Parameter Usage:','-Select library block in Component Browser','-Press Add Component','-Highlight component in Component Browser','-Select parameter in Parameter Browser','-Set parameter values','-Press Set Parameter'},'Invalid User Input','help'));
        return;
    end
    
    % get nominal value
    strNomValue = get(handles.NominalValue,'String');
    % test if nominal value is a double
    try
        setNomValue = eval(strNomValue);        
    catch
        uiwait(msgbox({'Set Parameter Usage:','Nominal value must be of class double to set'},'Invalid User Input','help'));
        handles = UpdateParNominalValue(handles);
        guidata(hObject,handles);
        return;
    end
    if ~isequal(class(setNomValue),'double')
        uiwait(msgbox({'Set Parameter Usage:','Nominal value must be of class double to set'},'Invalid User Input','help'));
        handles = UpdateParNominalValue(handles);
        guidata(hObject,handles);
        return;
    end   
    dblNomValue = setNomValue;
    % test nominal value size
    setNomSize = eval(['handles.data.sys.',strParPtrStrc,'.size']);
    if isequal(class(setNomSize),'double')    
        dblNomSize = setNomSize;
        if ~isequal(size(dblNomValue),dblNomSize)
            uiwait(msgbox({'Set Parameter Usage:','Nominal value must be of size specified on the R.H.S. of the nominal value'},'Invalid User Input','help'));
            handles = UpdateParNominalValue(handles);
            guidata(hObject,handles);
            return;
        end        
    elseif ~ischar(setNomSize)
        uiwait(msgbox({'Set Parameter:','Could not recognise parameter size class'},'Invalid User Input','warn'));
        handles = UpdateParNominalValue(handles);
        guidata(hObject,handles);
        return;        
    end
    % set nominal value
    eval(['handles.data.sys.',strParPtrStrc,'.nom = ',mat2str(dblNomValue),';']);
    eval(['handles.data.sys.',strParPtrStrc,'.val = ',mat2str(dblNomValue),';']);
        
    % test if delta value set and if of class double
    strDeltaValue = get(handles.DeltaValue,'String');
    if ~isequal(strDeltaValue,'Delta Value')
        try
            setDeltaValue = eval(strDeltaValue);        
        catch
            uiwait(msgbox({'Set Parameter Usage:','Delta value must be of class double to set'},'Invalid User Input','help'));
            handles = UpdateParDeltaValue(handles);
            guidata(hObject,handles);
            return;
        end
        if ~isequal(class(setDeltaValue),'double')
            uiwait(msgbox({'Set Parameter Usage:','Delta value must be of class double to set'},'Invalid User Input','help'));
            handles = UpdateParDeltaValue(handles);
            guidata(hObject,handles);
            return;
        end
        dblDeltaValue = setDeltaValue;

        % test if delta function exists in the matlab path
        cellDeltaFunction = get(handles.DeltaFunctionBrowser,'String');
        strDeltaFunction = cellDeltaFunction{get(handles.DeltaFunctionBrowser,'Value')};
        if ~IsFunc(strDeltaFunction)
            uiwait(msgbox({'Set Parameter Usage:','Delta function must be an existing function on the Matlab path.','Add delta function using the drop-down list.'},'Invalid User Input','help'));
            return;
        end

        % test if Delta Function can be evaluated
        try
            setPertValue = eval([strDeltaFunction,'(',mat2str(dblNomValue),',',mat2str(dblDeltaValue),');']);     
        catch
            uiwait(msgbox({'Set Parameter Usage:','Delta function must be evaluable with nominal and delta values (Delta Function error occurred).'},'Invalid User Input','help'));
            return;
        end
        % test if Delta Function output is of correct size
        if ~isequal(size(setPertValue),dblNomSize)
            uiwait(msgbox({'Set Parameter Usage:','Delta function must return double equal in size to nominal value size'},'Invalid User Input','help'));
            return;
        end

        % set delta value and delta function
        eval(['handles.data.sys.',strParPtrStrc,'.delta = ',mat2str(dblDeltaValue),';']);
        eval(['handles.data.sys.',strParPtrStrc,'.deltaFunction = ''',strDeltaFunction,''';']);
    end    
    
    % update parameter attributes
    handles = UpdateParAttribute(handles);
    
    % save gui data
    guidata(hObject,handles);
    
    return;
    

%% Helper functions
function handles = NewSystem(handles)
    % Get home directory
    strSysDir = uigetdir(pwd,'Select System Directory');
    if ~isequal(strSysDir,0)
        if IsDir(strSysDir)
            if isempty(GetChildDir(strSysDir)) && isempty(GetChildBlock(strSysDir))                
                strSysDir = '';
                uiwait(msgbox({'Select System Directory Usage:','System directory selected must contain','subdirectories and/or libraries with blocks.'},'Invalid User Input','help'));
            end
        else
            strSysDir = '';
            uiwait(msgbox({'Select System Directory Usage:','User must select existing directory.'},'Invalid User Input','help'));
        end
    else
        strSysDir = '';
        uiwait(msgbox({'Select System Directory Usage:','User must select existing directory,','which contains subdirectories and/or libraries with blocks.'},'Invalid User Input','help'));
    end
    % Create new system structures
    if ~isempty(strSysDir)
        % Remove path of previous system and add path of current system
        if isfield(handles,'data')
            if isfield(handles.data,'strSysDir')
                rmpath(genpath(handles.data.strSysDir))
            end
        end
        handles.data.strSysDir = strSysDir;    
        addpath(genpath(handles.data.strSysDir));    
        % initialise
        handles.data.sys = struct();
        handles.data.sys.info.name = '';
        handles.data.sys.info.dir = handles.data.strSysDir;
        % Test: set handles.data.sys.Satellite.Mechanics.Attitude.Mechanics.block='V:\Tools\AodcsSimulator\Satellite\Mechanics\Attitude\rigidBodyRotationalMechanics/Euler Attitude Dynamics';
        handles.data.cmp = handles.data.strSysDir; 
        handles.data.strDeltaFunction = {};
        handles = UpdateCmpBrowser(handles);
    end
    return;

function handles = UpdateCmpBrowser(handles)
    % Updates Component Browser with the current component's children
    if ~isempty(handles.data.cmp)
        child = GetChild(handles.data.cmp,handles.data.strSysDir,handles.data.sys);
        set(handles.ComponentBrowser,'String',StrRemovePath(child));
        set(handles.ComponentBrowser,'Value',size(child,1));
    else
        child = {'Component Browser'};
        set(handles.ComponentBrowser,'String',child);
        set(handles.ComponentBrowser,'Value',1);
    end
    handles = UpdateParBrowser(handles);
    return;

function handles = UpdateParBrowser(handles)
    % Updates Parameter Browser, according to selected component in Component Browser, with input parameters
    % Get current component
    strCmpPtrFile = GetSelectComponent(handles);
    % Test if current component exists
    if IsCmp(strCmpPtrFile,handles.data.strSysDir,handles.data.sys)
        % Get current cmp block parameters
        par = GetParBlock(StrGetPath(strCmpPtrFile,'slash',':'));
        % Generate parameter display string
        parDisp = {};
        for oPar=1:length(par)
            parDisp{end+1} = ['{',GetPortId(par{oPar}),'} "',GetPortDisc(par{oPar}),'"'];
        end
        if isempty(parDisp)
            parDisp = {'Parameter Browser'};
        end
        % Display parameters
        set(handles.ParameterBrowser,'String',StrRemovePath(parDisp));
        set(handles.ParameterBrowser,'Value',size(parDisp,1));
        % Highlight top parameter for quick access
    else
        set(handles.ParameterBrowser,'String',{'Parameter Browser'});
    end
    handles = UpdateParAttribute(handles);
    return;
 
function handles = UpdateParAttribute(handles)
    
    handles = UpdateParValue(handles);
    handles = UpdateParUnitSize(handles);
    handles = UpdateDeltaFunctionBrowser(handles);
    return;
    
function handles = UpdateParValue(handles)    
    handles = UpdateParNominalValue(handles);
    handles = UpdateParDeltaValue(handles);
    return;
    
function handles = UpdateParDeltaValue(handles)
    % function updates the nominal and delta values of the current selected parameter
    % get parameter from parameter browser
    strParPtrStrc = GetSelectParameter(handles);
    if isempty(strParPtrStrc)
        strDeltaValue = 'Delta Value';
    else
        % Get nominal and delta value
        sys = handles.data.sys;        
        if ExistStruct([strParPtrStrc,'.delta'],sys)
            eval(['strDeltaValue = mat2str(sys.',strParPtrStrc,'.delta);']);
        else
            strDeltaValue = 'Delta Value';
        end
    end
    set(handles.DeltaValue,'String',strDeltaValue);
    
	return;
    
function handles = UpdateParNominalValue(handles)
    % function updates the nominal value of the current selected parameter
    % get parameter from parameter browser
    strParPtrStrc = GetSelectParameter(handles);
    if isempty(strParPtrStrc)
        strNomValue = 'Nominal Value';
    else
        % Get nominal value
        sys = handles.data.sys;
        if ExistStruct([strParPtrStrc,'.nom'],sys)
            eval(['strNomValue = mat2str(sys.',strParPtrStrc,'.nom);']);
        else
            strNomValue = 'Nominal Value';
        end        
    end
    set(handles.NominalValue,'String',strNomValue);
    
	return;    

    
function handles = UpdateDeltaFunctionBrowser(handles)
    
    if  isempty(handles.data.strDeltaFunction)
        set(handles.DeltaFunctionBrowser,'String',{'Delta Function Browser','<add>'});
        set(handles.DeltaFunctionBrowser,'Value',1);
    else
        set(handles.DeltaFunctionBrowser,'String',{handles.data.strDeltaFunction{:},'<add>'});
        set(handles.DeltaFunctionBrowser,'Value',1);
    end

    return;
    
function handles = UpdateParUnitSize(handles)
    % function updates the unit and size of the current selected parameter
    % get parameter from parameter browser
    strParPtrStrc = GetSelectParameter(handles);
    if isempty(strParPtrStrc)
        set(handles.UnitSize,'String',{'(Unit) [Size]'});
        return;
    end
    % Get nominal and delta value
    sys = handles.data.sys;
    if ExistStruct([strParPtrStrc,'.unit'],sys)
        parUnit = ['(',eval(['sys.',strParPtrStrc,'.unit']),')'];
    else
        parUnit = '(Unit)';
    end
    if ExistStruct([strParPtrStrc,'.size'],sys)
        parSize = strrep(mat2str(eval(['sys.',strParPtrStrc,'.size'])),' ','x');
    else
        parSize = '[Size]';
    end
    set(handles.UnitSize,'String',{[parUnit,' ',parSize]});
    
	return;  

function strCmpPtrFile = GetSelectComponent(handles)
    % Get current selected component from Component Browser
    cellCmpList = get(handles.ComponentBrowser,'String');
    strCmpSelect = cellCmpList{get(handles.ComponentBrowser,'Value')};
    if ~isequal(strCmpSelect,'Component Browser')
        strCmpPtrFile = [handles.data.cmp,'\',strCmpSelect];
    else
        strCmpPtrFile = '';
    end
    return;
    
function strParPtrStrc = GetSelectParameter(handles)
    % Get current selected component from Component Browser
    strCmpPtrFile = GetSelectComponent(handles);
    [bIsCmp,strCmpPtrStrc] = IsCmp(strCmpPtrFile,handles.data.strSysDir,handles.data.sys);
    if bIsCmp
        % Get current parameter from Parameter Browser
        getParList = get(handles.ParameterBrowser,'String');
        if isequal(class(getParList),'cell')
            strParSelect = getParList{get(handles.ParameterBrowser,'Value')};
        else
            strParSelect = getParList;
        end
        % Set strParPtrStrc
        strParPtrStrc = [strCmpPtrStrc,'.',GetPortId(strParSelect)];
    else
        strParPtrStrc = '';
    end
    return;

function strCmpPtrFile = SetChild(strCmpPtrFile,child)
    strCmpPtrFile = [strCmpPtrFile,'\',StrRemovePath(child)];
    return

function strCmpPtrFile = SetParent(strCmpPtrFile)
    strCmpPtrFile = StrGetPath(strCmpPtrFile); 
    return;
    
    
