function varargout = SimulatorBuilder(varargin)
% All systems must have been build by SystemBuilder, and have at least 1 valid component.
% Option 1: Build simulation variable; sim = SimulatorBuilder('var',<Simulator name>,<System 1 mat>,<System 2 mat>,...,<System N mat>); Saves sim. All sys must have at least 1 valid component.
% Option 2: Build Simulink model; SimulatorBuilder('mdl',<Simulator name>); All sys must have at least 1 valid component.
% Include renaming option

% include paths of systems

%% initialise
% output
varargout = {};
% paths
strPathFile = which('SimulatorBuilder');
oAll = 1:length(strPathFile);
oSlash = oAll(strPathFile == '\');
strPath = strPathFile(1:(oSlash(end)-1));
addpath(strPath);
addpath([strPath,'\Libs']);

%% build variable sim
if isequal(lower(varargin{1}),'var')
    % check input size
    cIn = length(varargin);
    if cIn < 3
        error('Function (varargin{1} = ''var'') undefined for input size < 3.')
    end
    
    % simulator name
    strSim = varargin{2};
    if ~isequal(class(strSim),'char')
        error('Function undefined for input 2 not of class ''char''.');
    end
    
    % system names
    cSys = cIn - 2;  
    cellSysPath = cell(cSys);
    for oSys=1:cSys
        % clear variable sys
        clear sys;
        % load system
        strSys = varargin{oSys+2};
        if exist([strSys,'.mat'],'file') ~= 2
            error(['System ',strSys,' not found in Matlab path.']);            
        end
        try
            load(strSys);
        catch
            error(['Workspace ',strSys,' could not be loaded.']);
        end
        if exist('sys','var') ~= 1
            error(['System ',strSys,' does not contain sys variable.']);
        end       
        
        % test system
        [cellStrStrcCmp,cellStrStrcEmpty] = StrcSysToCellStrCmp(sys);
        if isempty(cellStrStrcCmp)
            error(['Variable ''sys'' in workspace ''',strSys,''' does not contain any valid components. Please load ''',strSys,''' in SystemBuilder.m and fix.']);
        end
        if ~isempty(cellStrStrcEmpty)
            warning(['Variable ''sys'' in workspace ''',strSys,''' contains invalid components. Only valid components will be used. Please load ''',strSys,''' in SystemBuilder.m and fix.']);
        end
        
        % add system
        sim.sysName{oSys} = strSys;
        % add system path
        cellSysPath{oSys} = sys.info.dir;
    end    
    
    % add paths of systems if no errors occured
    for oSys=1:cSys
        try
            addpath(genpath(cellSysPath{oSys}));
        catch
            warning(['Could not add system directory ''',cellSysPath{oSys},''' to Matlab path.']);
        end
    end
    
    save(strSim,'sim');
    varargout = {sim};
end


%% Build Simulator model
if isequal(lower(varargin{1}),'mdl')
    
    % check input size
    cIn = length(varargin);
    if cIn ~= 2
        error('Function (varargin{1} = ''mdl'') undefined for input size ~= 2.')
    end
    
    [sim,strSim,cellStrSysInvalidCmp] = SimulatorInterface('Load',varargin{2:end});
    
    % check with user if ok to use cleaned systems
    if ~isempty(cellStrSysInvalidCmp)
        [oSelect,bSelect] = listdlg('ListString',{'Continue','Abort'},'SelectionMode','single','InitialValue',[1],'Name','Model Build','PromptString',[{'The systems selected to load has unresolved fields:'},{cellStrSysInvalidCmp{:}},{' '},{'Use cleaned systems?'},{' '},{' '},{' '}],'ListSize',[300,300]);
        if (bSelect == 0) || (oSelect == 2)
            uiwait(msgbox({'System Load Aborted.','Model Build Aborted.'},'Model Build','none'));
            return;
        end
        uiwait(msgbox('Cleaned systems used for model build','Model Build','warn'));    
    end
    
    % create model if it doesn't exist
    try
        load_system(strSim);
        [oSelect,bSelect] = listdlg('ListString',{'Continue','Abort'},'SelectionMode','single','InitialValue',[1],'Name','Model Build','PromptString',[{['The Simulator model ',strSim,' already exists.']},{'Do you want to continue to update the model?'},{' '},{' '},{' '}],'ListSize',[300,300]);
        if (bSelect == 0) || (oSelect == 2)
            uiwait(msgbox({'Model Build Aborted.'},'Model Build','none'));
            return;
        end
    catch
        new_system(strSim);
        load_system(strSim);
    end
    
    % add model description
    strDescr = get_param(strSim,'Description');
    strDescr = [strDescr,'This model was created/modified by SimulatorBuilder ',SimulatorToolboxVersion(),' on ',datestr(now),'.'];
    set_param(strSim,'Description',strDescr);
    
    % add PreLoadFcn to model
    strPreLoadFcn = ['sim = SimulatorInterface(''Load'',''',strSim,''');'];
    getPreLoadFcn = get_param(strSim,'PreLoadFcn');  
    oStr = strfind(getPreLoadFcn,strPreLoadFcn);
    if isempty(oStr)
        if isempty(getPreLoadFcn)
            set_param(strSim,'PreLoadFcn',strPreLoadFcn);
        else
            set_param(strSim,'PreLoadFcn',[getPreLoadFcn,strPreLoadFcn]);
        end
    end
    
    % add CloseFcn to model
    strCloseFcn = 'clear sim;';
    getCloseFcn = get_param(strSim,'CloseFcn');
    lstrCloseFcn = strcmp(getCloseFcn,strCloseFcn);
    if sum(lstrCloseFcn) == 0
        if isempty(getCloseFcn)
            set_param(strSim,'CloseFcn',strCloseFcn);
        elseif isequal(class(getCloseFcn),'char')
            set_param(strSim,'CloseFcn',{getCloseFcn,strCloseFcn});
        elseif isequal(class(getCloseFcn),'cell')
            set_param(strSim,'CloseFcn',{getCloseFcn{:},strCloseFcn});
        else
            error('This should not happen.');
        end        
    end
    
    % iterate through each system, and add library blocks, parameter constant blocks and busses if they do not exist
    cellModelBlocks = find_system(strSim, 'FollowLinks', 'on', 'Type', 'block');
    cSys = length(sim.sysName);
    for oSys=1:cSys
        cellStrStrcCmp = StrcSysToCellStrCmp(eval(['sim.sys.',sim.sysName{oSys}]));
        cCmp = length(cellStrStrcCmp);
        % iterate through each component
        for oCmp=1:cCmp
            strLibBlock = eval(['sim.sys.',sim.sysName{oSys},'.',cellStrStrcCmp{oCmp},'.block']);
            strCmpBlock = [strSim,'/',sim.sysName{oSys},'/',strrep(cellStrStrcCmp{oCmp},'.','/')];
            load_system(StrGetPath(strLibBlock,'slash','/'));
            % iterate through each subsystem
            cDepth = sum(strCmpBlock=='/');
            for oDepth=1:cDepth
                if oDepth < cDepth
                    strCmpBlockSub = StrGetPath(strCmpBlock,'slash','/','pathDepthTopBottom',1+oDepth);
                else
                    strCmpBlockSub = strCmpBlock;
                end
                % check if subsystem or block already exists
                if sum(strcmp(cellModelBlocks,strCmpBlockSub)) == 0
                    % add subsystem block
                    if oDepth < cDepth
                        add_block('simulink/Ports & Subsystems/Subsystem',strCmpBlockSub);       
                    % add library block
                    else
                        add_block(strLibBlock,strCmpBlockSub);                        
                    end
                    cellModelBlocks{end+1} = strCmpBlockSub;
                end
                % check if parameter constant blocks already exists
                if oDepth == cDepth
                    % get parameters
                    cellStrSubBlock = find_system(strCmpBlock, 'SearchDepth', 1, 'FollowLinks', 'on', 'Type', 'block');
                    cellStrType = GetPortType(cellStrSubBlock);
                    lPar = strcmp(cellStrType,'par');       
                    olPar = LgcToOrd(lPar);
                    cellStrParPort = cellStrSubBlock(lPar);
                    cellStrParId = GetPortId(cellStrParPort);
                    cPar = sum(lPar);
                    for oPar=1:cPar
                        if isempty(cellStrParId{oPar})
                            uiwait(msgbox(['Block parameter ID not found. Make sure port ID''s of block ',strLibBlock,' are included in {}-brackets.'],'Build Model','help'));
                            return;
                        end
                        strParBlock = [StrGetPath(strCmpBlock,'slash','/'),'/',cellStrParId{oPar}];
                        % add block and connector if it doesn't exists
                        if sum(strcmp(cellModelBlocks,strParBlock)) == 0
                            add_block('simulink/Sources/Constant',strParBlock);
                            cellModelBlocks{end+1} = strParBlock;
                            strParBlockVal = ['sim.sys.',sim.sysName{oSys},'.',cellStrStrcCmp{oCmp},'.',cellStrParId{oPar},'.val'];
                            % get_param(strParBlock,'ObjectParameters')
                            set_param(strParBlock,'Value',strParBlockVal);
                            set_param(strParBlock,'VectorParams1D','off');
                            add_line(StrGetPath(strParBlock,'slash','/'),[cellStrParId{oPar},'/1'],[StrRemovePath(strCmpBlockSub,'slash','/'),'/',num2str(olPar(oPar)-1)]);
                        end                        
                    end
                end
                % check if bus creators already exists
                
                
            end
        end
    end
    
    % save system
    save_system(strSim);
end

end