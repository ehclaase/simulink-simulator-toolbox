Overview 	: 	v1.0
                Generate a Simulink Simulator with a user friendly MATLAB GUI. The Simulator Builder matches its Simulink subsystem structure with the existing system Simulink library folder structure.
				This library is developed to reduce implementation time of Simulink Simulators and to provide a user friendly means to view and change system parameters.
                
Version 	:   v0.1
                The user can run the System Builder GUI and create, save and load a system.
                The user can use the Simulator Builder MATLAB function to generate a simulator structure with multiple systems, 
                and generate a Simulink model and include all the blocks according to the structure of the systems included in the simulator structure.
                
                v0.2
                System parameters can be set to nominal values or perturbed values using library functions.
                Simulator parameters can be set to nominal values or perturbed values using library functions.
                Monte Carlo simulation can be ran using the Simulator Builder. 
                The Simulator Builder is implemented as a MATLAB function, since the Simulator Builder is not mature enough to have a GUI API.

                v1.0
				Documentation for this Mathworks Toolbox is included.
				
Usage 		:	Opening or loading the Simulator will automatically load the simulator and systems structures necessary for simulation. 
                While opened or loaded, the Simulator may be ran manually, or SimulatorApi.m may be used to set system parameters to nominal, 
                perturb parameters, or load saved system parameters of a previous simulation.

Licence 	:	This program is free software: you can redistribute it and/or modify
				it under the terms of the GNU General Public License as published by
				the Free Software Foundation, either version 3 of the License, or
				(at your option) any later version.

				This program is distributed in the hope that it will be useful,
				but WITHOUT ANY WARRANTY; without even the implied warranty of
				MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
				GNU General Public License for more details.

				You should have received a copy of the GNU General Public License
				along with this program.  If not, see <http://www.gnu.org/licenses/>.